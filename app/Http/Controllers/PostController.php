<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// the code below is the middleware used to get user_id, to verify user or anything that has to do with authentication.
use Illuminate\Support\Facades\Auth;
// Import Post model since we are going to need to use database.
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    // $request is the filled-up form
    public function store(Request $request)
    {
        // if there is an authenticated user
        if(Auth::user()) {
            // create a new Post object from the Post model
            $post = new Post;

            // define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            // save the post object to the database
            $post->save();

            return redirect('/posts');
        }else {
            return redirect('/login');
        }
    }

    public function index()
    {
        // get all posts from the database

        $posts = Post::where('isActive','=',true)->get();
        // $posts = Post::where('isActive',true)->get(); //this is also fine
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
    
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts()
    {
        // if the user is logged in
        if(Auth::user()){
            // retrieve user's own posts
            $posts = Auth::user()->posts; 

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    // note: $id shall correspond with the one between curly braces in routes file (web.php)
    public function show($id)
    {
        $post = Post::find($id);
        $postComments = PostComment::where('post_id', $id)->get(); 
        return view('posts.show')->with('post', $post)->with('postComments', $postComments);
    }

    public function edit($id)
    {
       $post = Post::find($id);
        return view('posts.edit')->with('post', $post); 
    }

    // pass both the form data in the request as well as the id of the post to be updated.
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        // if authenticated user's id is the same as the post's user id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }

        return redirect('/posts');
    }

    public function delete($id)
    {
        $post = Post::find($id);

        // if authenticated user's id is the same as the post's user id
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }

        return redirect('/posts');
    }

    public function archive($id)
    {
        $post = Post::find($id);

        // if authenticated user's id is the same as the post's user id
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }

        return redirect('/posts');
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // check if the authenticated user is NOT the post author
        if($post->user_id != $user_id){
            // check if a post has already been liked by this user
            if($post->likes->contains("user_id", $user_id)){
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                // if the user has not liked the post yet
                // create a new postlike object
                $postLike = new PostLike;
                // define the properties of the postlike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            // redirect the user to the specific post page
            // make sure to use double quotes when using parameterized route or even when just concatenating routes
            return redirect("/posts/$id");
        }
    }

    public function comment(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if(Auth::user()) {
            $postComment = new PostComment;

            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->content = $request->input('content');
    
            $postComment->save();

            return redirect("/posts/$id");
        }else {
            return redirect('/login');
        }
    }

}






