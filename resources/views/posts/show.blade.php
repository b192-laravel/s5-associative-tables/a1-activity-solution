@extends('layouts.app');

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Likes: {{count($post->likes)}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text text-muted">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif

			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
			  Post comment
			</button>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Comment on this post</h5>
			        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      </div>
			      <div class="modal-body">
			      	<form method="POST" action="/posts/{{$post->id}}/comment">
      					@csrf
			      		<div class="form-group">
			      		  <label for="content">Content:</label>
			      		  <textarea class="form-control" id="content" name="content" rows="3" placeholder="Type your comment here..."></textarea>
			      		</div>
			        	<div class="mt-2">
			        		<button type="submit" class="btn btn-primary">Post</button>
			        	</div>
			      	</form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
			      </div>
			    </div>
			  </div>
			</div>

			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>

			<h2 class="mt-3 text-center">Comments</h2>

			@if(count($postComments) > 0)
		        @foreach($postComments as $postComment)
		            <div class="card text-center mb-2">
		                <div class="card-body">
		                	<h6 class="card-text mb-3">{{$postComment->content}}</h6>
		                    <h6 class="card-text text-muted">Commented By: {{$postComment->user->name}}</h6>
		                    <p class="card-subtitle mb-3 text-muted">Commented at: {{$post->created_at}}</p>
		                </div>
		            </div>
		        @endforeach
		    @else
		        <div>
		            <h2>There are no comments to show</h2>
		        </div>
		    @endif
		</div>
	</div>
@endsection